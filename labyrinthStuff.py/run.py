import pygame as pg  
from random import randint
import Map




def randomColor():return (randint(0,255),randint(0,255),randint(0,255),randint(0,255))

def clearScreen(screen:pg.Surface):
    screen.fill((0,0,0))
    #I havent tested that this works yet. 


def displayMap(screen:pg.Surface,map:Map.Map):
    clearScreen(screen)
    screenProp=screen.get_size()
    xPixels=screenProp[0]
    yPixels=screenProp[1]

    xRects=map.xSize
    yRects=map.ySize
    
    xWidth=int(xPixels/xRects)
    yWidth=int(yPixels/yRects)

    #pg.draw.rect(screen,"red",pg.Rect(100,100,100,100))

    for x in range(xRects):
        for y in range(yRects):
            pg.draw.rect(screen,"brown",pg.Rect(x*xWidth,y*yWidth,xWidth,yWidth))
    for tuple in map.getPlayers():
        pg.draw.rect(screen,"green",pg.Rect(tuple[0]*xWidth,tuple[1]*yWidth,xWidth,yWidth))
    pg.draw.rect(screen,"cyan",pg.Rect(map.playerPos[0]*xWidth,map.playerPos[1],xWidth,yWidth))
    


def run():
    screen =pg.display.set_mode((400,400))
    clock=pg.time.Clock()


    aMap=Map.Map(2,4,4)

    done=False
    while not done:
        for event in pg.event.get():
            #print(event.type)

            #this is some really garbage code but I am working on it at least. 
            if event.type==pg.QUIT:
                done=True
            if event.type==pg.KEYDOWN:
                print("button pressed!")
                print(event.key)

                if event.key==1073741903:
                    print("right")
                    aMap.update(0)
                if event.key==1073741904:
                    print("left")
                    aMap.update(1)
                if event.key==1073741906:
                    print("up")
                    aMap.update(2)
                if event.key==1073741905:
                    print("down")
                    aMap.update(3)
                #I know that this isn't the most readable code and I will work on it. 
                #Something I need to think about:
                    #I think if's are probably bad but I dont know. I need to know more about this. 


            else: aMap.update(-1)
        #I think I like the codes and they make sense with the positions in the arrays. 
        #The challenge is just that it isn't consistent. 
        
        displayMap(screen,aMap)


        clock.tick(1)
        pg.display.update()
run()                