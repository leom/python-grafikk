import pygame as pg
from random import randint,choice


def add(a:tuple[int,int],b:tuple[int,int])->tuple[int,int]:
    return (a[0]+b[0],a[1]+b[1])

class Map:

    def __init__(self,playerAmount,xSize,ySize):
        self.xSize=xSize
        self.ySize=ySize
        self.playerArray=[self.randomPosition() for n in range(playerAmount)]
        self.playerPos=self.randomPosition()
        #this feels like short code but it also makes sense. 

    def randomPosition(self)->tuple[int,int]:
        xPos=randint(0,self.xSize-1)
        yPos=randint(0,self.ySize-1)
        #I add the -1 because randint adds the last number in the range. 
        return (xPos,yPos)

    def positionLegal(self,y,x):#I say y before x for practical reasons. 
        return x in range(self.xSize) and y in range(self.ySize)
        #x in range is certainly a piece of code that can be optimized.
            #I havent because this is very readable. 
        #the goal is that the update just calls this one for everything in the self.playerArray

    def update(self,move=-1):
        movements=[(0,1),(0,-1),(1,0),(-1,0)]#no particular order of these. 
        if move>=0:
            newPos=add(self.playerPos,movements[move])
            if self.positionLegal(newPos[0],newPos[1]):
                self.playerPos=newPos
                
        for ind,point in enumerate(self.playerArray):
            position=add(point,choice(movements))
            while not self.positionLegal(position[0],position[1]):#worst-case: O(inf). 
                position=add(point,choice(movements))
            self.playerArray[ind]=position
        #Damn I am guessing that this method has a lot of bad about it lol at least in running time. 
        #I chose this algorithm because I think removing things from the list is a bad idea. 


    def getPlayers(self)->list[tuple[int,int]]:
        return self.playerArray

#I think the next goal is to build a character that moves around as well. 
#To do this, I need the map to store the tuple as well. 
#I could even build a login screen and make that one work well too. 